from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from web.models import ManagementStaff,Gallery,Achievements,Events


def index(request):
    management_datas = ManagementStaff.objects.all()
    gallery_datas = Gallery.objects.all()
    achievements_datas = Achievements.objects.all()
    events_datas = Events.objects.all()
    context = {
        "title": "JNASC",
        "caption": "Jamia ",
        "management_datas" : management_datas,
        "gallery_datas" : gallery_datas,
        "achievements_datas" : achievements_datas,
        "events_datas" : events_datas,
        "is_home": True
     }
    return render(request,'web/index.html',context)


def aboutus(request):
    context = {
        "title": "About",
        "caption": "JNASC ",
        "is_aboutus": True
     }

    return render(request,'web/about-us.html',context)


def events(request):
    events_datas = Events.objects.all()
    context = {
        "title": "Events",
        "caption": "JNASC ",
        "events_datas" : events_datas,
        "is_events": True
     }
    return render(request,'web/events.html',context)


def staffs(request):
    management_datas = ManagementStaff.objects.all()
    context = {
        "title": "Staffs",
        "caption": "JNASC ",
        "management_datas" : management_datas,
        "is_staffs": True
     }
    return render(request,'web/staffs.html',context)

def gallery(request):
    gallery_datas = Gallery.objects.all()
    context = {
        "title": "Gallery",
        "caption": "JNASC ",
        "gallery_datas" : gallery_datas,
        "is_gallery": True
     }

    return render(request,'web/gallery.html',context)

def Acconts(request):
    context = {
        "title": "Acconts",
        "caption": "JNASC ",
        "is_aboutus": True
     }

    return render(request,'web/Acconts.html',context)
    
def attendance(request):
    context = {
        "title": "attendance",
        "caption": "JNASC ",
        "is_aboutus": True
     }

    return render(request,'web/attendance.html',context)