# Generated by Django 2.2.2 on 2019-06-25 10:02

from django.db import migrations, models
import versatileimagefield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Achievements',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('description', models.TextField()),
                ('image', models.ImageField(upload_to='web/achievements/')),
            ],
            options={
                'verbose_name': 'achievements',
                'verbose_name_plural': 'achievements',
                'db_table': 'web_achievements',
            },
        ),
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('date', models.DateTimeField()),
                ('description', models.TextField()),
                ('image', models.ImageField(upload_to='web/events/')),
                ('programs', models.CharField(choices=[('arts', 'Arts'), ('games', 'Games'), ('otheractivities', 'Otheractivities')], default='otheractivities', max_length=28)),
            ],
            options={
                'verbose_name': 'events',
                'verbose_name_plural': 'events',
                'db_table': 'web_events',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', versatileimagefield.fields.VersatileImageField(upload_to='web/gallery/', verbose_name='Image')),
            ],
            options={
                'verbose_name': 'gallery',
                'verbose_name_plural': 'gallery',
                'db_table': 'web_gallery',
            },
        ),
        migrations.CreateModel(
            name='ManagementStaff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('position', models.CharField(max_length=128)),
                ('image', models.ImageField(upload_to='web/management/')),
                ('role', models.CharField(choices=[('management', 'Management'), ('administrative', 'Administrative'), ('teacher', 'Teaching Staff'), ('collage_council', 'Collage Council')], default='TeachingStaff', max_length=28)),
            ],
            options={
                'verbose_name': 'management',
                'verbose_name_plural': 'management',
                'db_table': 'web_management',
            },
        ),
    ]
