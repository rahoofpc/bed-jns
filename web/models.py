from django.db import models
from django.utils.translation import ugettext_lazy as _
from versatileimagefield.fields import VersatileImageField

role_choices = [
    ('management', 'Management'),
    ('administrative', 'Administrative'),
    ('teacher', 'Teaching Staff'),
    ('students', 'Students')
]
programs_choices = [
    ('arts','Arts'),
    ('games','Games'),
    ('otheractivities','Otheractivities')
]
class ManagementStaff(models.Model):
    name = models.CharField(max_length=128)
    position = models.CharField(max_length=128)
    image = models.ImageField(upload_to="web/management/")
    role = models.CharField(max_length=28, choices=role_choices, default='TeachingStaff')

    class Meta:
        db_table = 'web_management'
        verbose_name = "management"
        verbose_name_plural = "management"

    def __str__(self):
        return self.name


class Gallery(models.Model):
    image = VersatileImageField('Image',upload_to='web/gallery/')

    class Meta:
        db_table = 'web_gallery'
        verbose_name = "gallery"
        verbose_name_plural = "gallery"

    def __str__(self):
        return self.image.url


class Achievements(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField()
    image = models.ImageField(upload_to="web/achievements/")
    class Meta:
        db_table = 'web_achievements'
        verbose_name = "achievements"
        verbose_name_plural = "achievements"

    def __str__(self):
        return self.name


class Events(models.Model):
    name = models.CharField(max_length=128)
    date = models.DateTimeField(blank=False)
    description = models.TextField()
    image = models.ImageField(upload_to="web/events/")
    programs = models.CharField(max_length=28,choices=programs_choices,default='otheractivities')
    class Meta:
        db_table = 'web_events'
        verbose_name = "events"
        verbose_name_plural = "events"

    def __str__(self):
        return self.name
