from __future__ import unicode_literals
from django.contrib import admin
from web.models import ManagementStaff,Gallery,Achievements,Events


class ManagementStaffAdmin(admin.ModelAdmin):
    list_display = ('name','position','image','role')

admin.site.register(ManagementStaff,ManagementStaffAdmin)


class GalleryAdmin(admin.ModelAdmin):
    display = ('image')

admin.site.register(Gallery,GalleryAdmin)


class AchievementsAdmin(admin.ModelAdmin):
    list_display = ('name','description','image')

admin.site.register(Achievements,AchievementsAdmin)


class EventsAdmin(admin.ModelAdmin):
    list_display = ('name','date','description','image','programs')

admin.site.register(Events,EventsAdmin)
