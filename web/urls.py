from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'web'
urlpatterns = [
    path('',views.index, name="index"),
    path('aboutus/',views.aboutus, name="aboutus"),
    path('events/',views.events, name="events"),
    path('staffs/',views.staffs, name="staffs"),
    path('gallery/',views.gallery, name="gallery"),
    path('Acconts/',views.Acconts, name="Acconts"),
    path('attendance/',views.attendance, name="attendance"),

]
