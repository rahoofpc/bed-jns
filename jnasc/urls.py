from django.contrib import admin
from django.conf.urls import url,include
from django.urls import path, re_path
from django.views.static import serve
from django.conf import settings


urlpatterns = [
    path('JNASC-admin/', admin.site.urls),
    path('',include('web.urls',namespace='web')),

    re_path('media/(?P<path>.*)$', serve, { 'document_root': settings.MEDIA_ROOT}),
    re_path('static/(?P<path>.*)$', serve, { 'document_root': settings.STATIC_FILE_ROOT}),

]
