function resize(){
    function equalCard(selector){
        var h = 0;
        $(selector).height('auto');
        $(selector).each(function(){
            var height = $(this).height();
            if(height > h){
                h = height;
            }
        });
        $(selector).height(h);
    }
    equalCard('#departments ul li');
    var window_width = $(window).width()
}

$(document).ready(function(){
  $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1750);
                return false;
            }
        }
    });

  $('[data-fancybox]').fancybox({
     loop: true,
     buttons : [
       'zoom',
       'thumbs',
       'fullScreen',
       'download',
       'close'
     ]
   });
    $('.icon-menu-outline').click(function(){
        $('.mobile-menu-overlay').css({
            "display" : "block"
        });
        $('div.mobile-menu').css({
            "right" : "0"
        });
        $('.icon-cancel-circled-outline').show();
    });

    $('.icon-cancel-circled-outline').click(function(){
        $('.mobile-menu-overlay').css({
            "display" : "none"
        });
        $('div.mobile-menu').css({
            "right" : "-300px"
        });
        $('.icon-cancel-circled-outline').hide();
    });

    $("#teams ul li div.container").hover(function(){
        $(this).parent().addClass("active");
    },function(){
        $(this).parent().removeClass("active");
    });

    $("#teamspage ul li div.container").hover(function(){
        $(this).parent().addClass("active");
    },function(){
        $(this).parent().removeClass("active");
    });

    $("#achivements ul.owl-carousel").owlCarousel({
       "loop" : true,
       "autoplay" : true,
       "items" : 3,
       "nav" : false,
       "margin" : 30,
       "responsive" : {
           // breakpoint from 0 up
           0 : {
             items : 1
           },
           // breakpoint from 480 up
           480 : {
             items : 2
           },
            // breakpoint from 640 up
           640 : {
             items : 2
           },
           // breakpoint from 768 up
           768 : {
           	 items : 2
           },
           980 : {
               items : 3
           },
           1050 : {
               items : 4
           }
       }

    });

    
    $('header section.bottom nav ul li.downloads').mouseenter(function(){
    $('header section.bottom nav ul li.downloads div.drop-down').css("display","block");
   });

    $('header section.bottom nav ul li.downloads').mouseleave(function(){
    $('header section.bottom nav ul li.downloads div.drop-down').css("display","none");
   });

    $("div.mobile-menu ul li.downloads").click(function(){
    $('div.mobile-menu ul li.downloads div.drop-down').toggleClass('remove');
   });
});

$(window).resize(function(){
    resize();
});

$(window).on('load',function(){
    resize();
});

$( function() {
	$( "#events" ).tabs (
   		{ show: { effect: "fade", duration: 1000 } }
	);
} );

$( function() {
  $( "#tabs" ).tabs();
} );

$( function() {
  $( "#eventtabs" ).tabs();
} );
